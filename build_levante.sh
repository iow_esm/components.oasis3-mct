#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0
module load intel-oneapi-mpi/2021.5.0-intel-2021.5.0

# GET IOW ESM ROOT PATH
export IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# compile mode: "PRODUCTION" or "DEBUG"
if [ $debug == "debug" ]; then
	export IOW_ESM_COMPILE_MODE="DEBUG"
elif [ $debug == "release" ]; then
	export IOW_ESM_COMPILE_MODE="PRODUCTION"
else
	echo "Compile mode is not specified correctly. Use debug or release"
	exit;
fi

# include paths
export IOW_ESM_NETCDFC_INCLUDE="/sw/spack-levante/netcdf-fortran-4.5.3-r5r3ev/include"
export IOW_ESM_NETCDFC_LIBRARY="/sw/spack-levante/netcdf-fortran-4.5.3-r5r3ev/lib"

# define compilers for MPI wrappers
export I_MPI_CC=icc                         # cmake and MPI build flags
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort

# executables
export IOW_ESM_MAKE="/usr/bin/make"
export IOW_ESM_FC="mpiifort"
export IOW_ESM_CC="mpiicc"
export IOW_ESM_LD="mpiifort"

# compiler flags
if [ $debug == "debug" ]; then
	export IOW_ESM_FFLAGS="-O0 -g -traceback -fno-alias -ip -align"
	export IOW_ESM_CFLAGS="-vec-report0 -O0 -fno-alias -ip -align"
	export IOW_ESM_LDFLAGS="-g -traceback"
else
	export IOW_ESM_FFLAGS="-O3 -r8 -O3 -no-prec-div -static -fp-model fast=2 -xHost"
	export IOW_ESM_CFLAGS="-vec-report0 -O3 -r8 -O3 -no-prec-div -static -fp-model fast=2 -xHost"
	export IOW_ESM_LDFLAGS=""
fi

# MAKE CLEAN
if [ $rebuild == "rebuild" ]; then
	rm -r ${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/IOW_ESM_${IOW_ESM_COMPILE_MODE}
fi

# RUN BUILD COMMAND
cd ${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/util/make_dir
${IOW_ESM_MAKE} --file=TopMakefileOasis3
cd ${IOW_ESM_ROOT}/components/OASIS3-MCT
