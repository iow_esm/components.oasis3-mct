#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module load intel-oneapi-compilers/2023.2.1
module load intel-oneapi-mpi/2021.10.0
module load netcdf-fortran/4.6.1-mpi

# search for netcdf-fortran paths in environment variables
export IOW_ESM_NETCDFC_INCLUDE=`env | grep ^INCLUDE= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf-fortran"){print $i}}}'`
export IOW_ESM_NETCDFC_LIBRARY=`env | grep ^LIBRARY_PATH= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf-fortran"){print $i}}}'`

export IOW_ESM_FC="mpiifort"
export IOW_ESM_CC="mpiicc"
export IOW_ESM_LD="mpiifort"

export IOW_ESM_MAKE="/usr/bin/make"

# GET IOW ESM ROOT PATH
export IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# compile mode: "PRODUCTION" or "DEBUG"
if [ $debug == "debug" ]; then
	export IOW_ESM_COMPILE_MODE="DEBUG"
elif [ $debug == "release" ]; then
	export IOW_ESM_COMPILE_MODE="PRODUCTION"
else
	echo "Compile mode is not specified correctly. Use debug or release"
	exit;
fi

# compiler flags
if [ $debug == "debug" ]; then
	export IOW_ESM_FFLAGS="-O0 -g -traceback -fno-alias -ip -align"
	export IOW_ESM_CFLAGS="-vec-report0 -O0 -fno-alias -ip -align"
	export IOW_ESM_LDFLAGS="-g -traceback"
else
	export IOW_ESM_FFLAGS="-O3 -r8"
	export IOW_ESM_CFLAGS="-O3 "
	export IOW_ESM_LDFLAGS=""
fi

# MAKE CLEAN
if [ $rebuild == "rebuild" ]; then
	rm -r ${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/IOW_ESM_${IOW_ESM_COMPILE_MODE}
fi

# RUN BUILD COMMAND
cd ${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/util/make_dir
${IOW_ESM_MAKE} --file=TopMakefileOasis3
cd ${IOW_ESM_ROOT}/components/OASIS3-MCT
