#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module load intel/2022.2/compiler/2022.1.0
module load intel/2022.2/mpi/2021.6.0
module load netcdf/4.9.2-intel_2022.2

# search for netcdf-fortran paths in environment variables
export IOW_ESM_NETCDFC_INCLUDE="/sw/data/netcdf/OS_15.5/x86_64-suse-linux/4.9.2/intel_2022/include"
export IOW_ESM_NETCDFC_LIBRARY="/sw/data/netcdf/OS_15.5/x86_64-suse-linux/4.9.2/intel_2022/lib64"

export IOW_ESM_FC="mpiifort"
export IOW_ESM_CC="mpiicc"
export IOW_ESM_LD="mpiifort"

export IOW_ESM_MAKE="/usr/bin/make"

# GET IOW ESM ROOT PATH
export IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# compile mode: "PRODUCTION" or "DEBUG"
if [ $debug == "debug" ]; then
	export IOW_ESM_COMPILE_MODE="DEBUG"
elif [ $debug == "release" ]; then
	export IOW_ESM_COMPILE_MODE="PRODUCTION"
else
	echo "Compile mode is not specified correctly. Use debug or release"
	exit;
fi

# compiler flags
if [ $debug == "debug" ]; then
	export IOW_ESM_FFLAGS="-O0 -g -traceback -fno-alias -ip -align"
	export IOW_ESM_CFLAGS="-vec-report0 -O0 -fno-alias -ip -align"
	export IOW_ESM_LDFLAGS="-g -traceback"
else
	export IOW_ESM_FFLAGS="-O3 -r8"
	export IOW_ESM_CFLAGS="-O3 "
	export IOW_ESM_LDFLAGS=""
fi

# MAKE CLEAN
if [ $rebuild == "rebuild" ]; then
	rm -r ${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/IOW_ESM_${IOW_ESM_COMPILE_MODE}
fi

# RUN BUILD COMMAND
cd ${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/util/make_dir
${IOW_ESM_MAKE} --file=TopMakefileOasis3
cd ${IOW_ESM_ROOT}/components/OASIS3-MCT
